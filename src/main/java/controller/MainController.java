package controller;

import code.Game;
import code.Tile;
import code.TileManager;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController {

    @FXML private AnchorPane apCenter;
    @FXML private Pane pane;
    @FXML private Button bReset, bOptions;
    @FXML private Text tStatus, tMines, tTime;

    private int WIDTH, HEIGHT;

    private Game game;
    private Stage stage;
    private AnimationTimer animationTimer;

    @FXML
    public void initialize() {

        //Display current time elapsed from clicking the first tile

        animationTimer = new AnimationTimer(){
            @Override
            public void handle(long l) {
                String pb = " / (PB: " + game.getRecords().getBestTime(game.getDifficulty()) + ")";
                if(!game.isActiveGame()){
                    setTimeText(Integer.toString((int)(game.getEndTime() - game.getStartTime()) / 1000), pb);
                }else if(game.getStartTime() == 0){
                    setTimeText("0", pb);
                }else{
                    setTimeText(Integer.toString((int)(System.currentTimeMillis() - game.getStartTime()) / 1000), pb);
                }
            }
        };

        pane.setOnMouseClicked(event -> {
            if(game.isActiveGame()){
                Tile tile = game.getTileManager().getTile((int)(Math.round((event.getX() - ((int)(TileManager.TILE_SIZE / 2))) / TileManager.TILE_SIZE) * TileManager.TILE_SIZE) / TileManager.TILE_SIZE,
                                          (int)(Math.round((event.getY() - ((int)(TileManager.TILE_SIZE / 2))) / TileManager.TILE_SIZE) * TileManager.TILE_SIZE) / TileManager.TILE_SIZE);
                if(event.getButton() == MouseButton.PRIMARY){
                    if(tile != null && tile.isClickable()){
                        game.processTile(tile);
                    }
                }else if(event.getButton() == MouseButton.SECONDARY){
                    if(tile != null){
                        game.markTile(tile);
                    }
                }
                game.checkForVictory();
            }
        });
    }

    public void setTimeText(String time, String pb){
        tTime.setText("Time: " + time + pb);
    }

    public void adjustNodes(){
        bReset.setLayoutX(WIDTH - bReset.getPrefWidth() - 20);
        tStatus.setLayoutX((int)(WIDTH / 2) - (tStatus.getLayoutBounds().getWidth() / 2));
        tStatus.setLayoutY(HEIGHT - 5);
        bOptions.setLayoutX(WIDTH - bOptions.getPrefWidth() - 20);
    }

    public void updateTile(Tile tile){
        Image image = tile.getProperTileImage(game.isActiveGame());
        if(image != null)
            tile.getRectangle().setFill(new ImagePattern(tile.getProperTileImage(game.isActiveGame())));
    }

    public void drawTiles(){
        for (Tile tile : game.getTileManager().getAllTiles()) {
            Rectangle rec = new Rectangle(tile.getX() * TileManager.TILE_SIZE, tile.getY() * TileManager.TILE_SIZE, TileManager.TILE_SIZE, TileManager.TILE_SIZE);
            rec.setFill(new ImagePattern(tile.getProperTileImage(game.isActiveGame())));
            tile.setRectangle(rec);
            pane.getChildren().add(rec);
        }
    }

    public void bResetOnAction(){
        pane.getChildren().clear();
        setStatusText("");
        game.startNewGame();
    }

    public void resize(int width, int height){
        WIDTH = 40 + (width * TileManager.TILE_SIZE);
        HEIGHT = 100 + (height * TileManager.TILE_SIZE);
        stage.setWidth(WIDTH + 16);
        stage.setHeight(HEIGHT + 39);
        adjustNodes();
    }

    public void setStatusText(String text){
        tStatus.setText(text);
    }

    public void setMinesText(String text){
        tMines.setText("Mines: " + text);
    }

    public void bOptionsOnAction(){
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Options.fxml"));
            Parent root = fxmlLoader.load();
            Stage previousStage = (Stage) bOptions.getScene().getWindow();

            Scene scene = new Scene(root, 266, 268);
            Stage stage = new Stage();

            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(previousStage);

            stage.getIcons().add(new Image(String.valueOf(getClass().getResource("/res/icon.png"))));
            stage.setTitle("Options");
            stage.setResizable(false);
            stage.setScene(scene);

            OptionsController optionsController = fxmlLoader.getController();
            optionsController.setMainController(this);
            optionsController.setGame(game);

            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AnimationTimer getAnimationTimer() {
        return animationTimer;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public int getWIDTH() {
        return WIDTH;
    }

    public void setWIDTH(int WIDTH) {
        this.WIDTH = WIDTH;
    }

    public int getHEIGHT() {
        return HEIGHT;
    }

    public void setHEIGHT(int HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
