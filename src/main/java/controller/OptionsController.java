package controller;

import code.Difficulty;
import code.Game;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class OptionsController {

    @FXML private AnchorPane apMain, apCustom;
    @FXML private RadioButton rbEasy, rbMedium, rbHard, rbCustom;
    @FXML private TextField tfWidth, tfHeight, tfMines;
    @FXML private Button bConfirm;
    @FXML private CheckBox cbEasy;

    private Difficulty difficulty;

    private Game game;
    private MainController mainController;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            if(game.getDifficulty() == Difficulty.EASY){
                difficulty = Difficulty.EASY;
                rbEasy.setSelected(true);
            }else if(game.getDifficulty() == Difficulty.MEDIUM){
                difficulty = Difficulty.MEDIUM;
                rbMedium.setSelected(true);
            }else if(game.getDifficulty() == Difficulty.HARD){
                difficulty = Difficulty.HARD;
                rbHard.setSelected(true);
            }else{
                difficulty = Difficulty.CUSTOM;
                rbCustom.setSelected(true);
                apCustom.setVisible(true);
            }

            cbEasy.setSelected(game.isEasyMode());
            cbEasy.setTooltip(new Tooltip("Guarantees that first clicked tile is not a mine."));

            rbEasy.setTooltip(new Tooltip("30 mines"));
            rbMedium.setTooltip(new Tooltip("60 mines"));
            rbHard.setTooltip(new Tooltip("120 mines"));

            tfMines.setText(Integer.toString(game.getNumOfMines()));
            tfWidth.setText(Integer.toString(game.getNumOfTilesX()));
            tfHeight.setText(Integer.toString(game.getNumOfTilesY()));

            setTooltips();

            numOnlyTextFields(tfHeight);
            numOnlyTextFields(tfWidth);
            numOnlyTextFields(tfMines);
        });
    }

    public void setTooltips(){
        if(!tfHeight.getText().isEmpty() && !tfWidth.getText().isEmpty()){
            tfMines.setTooltip(new Tooltip("1 - " + (((Integer.parseInt(tfHeight.getText()) * Integer.parseInt(tfWidth.getText())) == 1) ? "1" : (Integer.parseInt(tfHeight.getText()) * Integer.parseInt(tfWidth.getText()) - 1))));
        }
        tfWidth.setTooltip(new Tooltip("6 - 35"));
        tfHeight.setTooltip(new Tooltip("6 - 35"));
    }

    public void bConfirmOnAction(){

        if(difficulty == Difficulty.CUSTOM){
            String message = "";
            if(Integer.parseInt(tfWidth.getText()) > 35)
                message = "Maximum width is 35!";
            if(Integer.parseInt(tfHeight.getText()) > 35 && message.isEmpty())
                message = "Maximum height is 35!";
            if(Integer.parseInt(tfWidth.getText()) < 6 && message.isEmpty())
                message = "Minimum width is 6!";
            if(Integer.parseInt(tfHeight.getText()) < 6 && message.isEmpty())
                message = "Minimum height is 6!";
            if(Integer.parseInt(tfMines.getText()) < 1 && message.isEmpty())
                message = "Minimum mines is 1!";
            if(Integer.parseInt(tfMines.getText()) > (Integer.parseInt(tfHeight.getText()) * Integer.parseInt(tfWidth.getText())) && message.isEmpty())
                message = "Maximum mines is " + (Integer.parseInt(tfHeight.getText()) * Integer.parseInt(tfWidth.getText()) - 1) + "!";
            if(!message.isEmpty()){
                Alert alert = new Alert(Alert.AlertType.NONE, message, ButtonType.OK);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK) {
                    alert.close();
                }
            }else{
                game.setNumOfTilesX(Integer.parseInt(tfWidth.getText()));
                game.setNumOfTilesY(Integer.parseInt(tfHeight.getText()));
                game.setNumOfMines(Integer.parseInt(tfMines.getText()));
                game.setDifficulty(difficulty);
                game.setEasyMode(cbEasy.isSelected());
                mainController.bResetOnAction();
                mainController.resize(game.getNumOfTilesX(), game.getNumOfTilesY());
                Stage stage = (Stage)apMain.getScene().getWindow();
                stage.close();
            }
        }else{
            if(game.getDifficulty() == Difficulty.CUSTOM){
                mainController.resize(game.getNumOfTilesX(), game.getNumOfTilesY());
            }
            if(difficulty == Difficulty.EASY){
                game.setNumOfMines(30);
            }else if(difficulty == Difficulty.MEDIUM){
                game.setNumOfMines(60);
            }else if(difficulty == Difficulty.HARD){
                game.setNumOfMines(120);
            }
            game.setDifficulty(difficulty);
            game.setEasyMode(cbEasy.isSelected());
            mainController.bResetOnAction();
            Stage stage = (Stage)apMain.getScene().getWindow();
            stage.close();
        }
    }

    public void rbOnAction(ActionEvent e){
        switch (((RadioButton) e.getSource()).getId()) {
            case "rbEasy":
                difficulty = Difficulty.EASY;
                rbEasy.setSelected(true);
                rbMedium.setSelected(false);
                rbHard.setSelected(false);
                rbCustom.setSelected(false);
                apCustom.setVisible(false);
                break;
            case "rbMedium":
                difficulty = Difficulty.MEDIUM;
                rbEasy.setSelected(false);
                rbMedium.setSelected(true);
                rbHard.setSelected(false);
                rbCustom.setSelected(false);
                apCustom.setVisible(false);
                break;
            case "rbHard":
                difficulty = Difficulty.HARD;
                rbEasy.setSelected(false);
                rbMedium.setSelected(false);
                rbHard.setSelected(true);
                rbCustom.setSelected(false);
                apCustom.setVisible(false);
                break;
            case "rbCustom":
                difficulty = Difficulty.CUSTOM;
                rbEasy.setSelected(false);
                rbMedium.setSelected(false);
                rbHard.setSelected(false);
                rbCustom.setSelected(true);
                apCustom.setVisible(true);
                break;
        }
    }

    public void numOnlyTextFields(TextField tf) {
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tf.setText(oldValue);
            }
            setTooltips();
        });
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
