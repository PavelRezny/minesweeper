module minesweeper {
    requires javafx.controls;
    requires javafx.fxml;

    opens code to javafx.fxml;
    opens controller to javafx.fxml;
    exports code;
    exports controller;
}