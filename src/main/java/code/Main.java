package code;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
        Parent root = fxmlLoader.load();

        MainController mainController = fxmlLoader.getController();
        mainController.setStage(primaryStage);

        TileManager tileManager = new TileManager();
        Records records = new Records();
        GameGenerator gameGenerator = new GameGenerator(tileManager);

        records.readRecords();

        Game game = new Game(tileManager, records, gameGenerator, mainController);

        mainController.setGame(game);

        mainController.setWIDTH(40 + (game.getNumOfTilesX() * TileManager.TILE_SIZE));
        mainController.setHEIGHT(100 + (game.getNumOfTilesY() * TileManager.TILE_SIZE));

        game.startNewGame();

        primaryStage.getIcons().add(new Image(String.valueOf(getClass().getResource("/res/icon.png"))));
        primaryStage.setTitle("Minesweeper");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, mainController.getWIDTH(), mainController.getHEIGHT()));
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
