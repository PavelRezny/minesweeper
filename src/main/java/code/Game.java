package code;

import controller.MainController;

import java.util.Stack;

public class Game {

    private boolean activeGame = true;

    private int numOfMines = 30;
    private int numOfTilesX = 15, numOfTilesY = 15;

    private long startTime = 0;
    private long endTime = 0;

    private Difficulty difficulty = Difficulty.EASY;

    private boolean easyMode = false;

    private int overallMinesMarked = 0;

    private final Records records;
    private final TileManager tileManager;
    private final GameGenerator gameGenerator;
    private final MainController mainController;

    public Game(TileManager tileManager, Records records, GameGenerator gameGenerator, MainController mainController){
        this.tileManager = tileManager;
        this.records = records;
        this.gameGenerator = gameGenerator;
        this.mainController = mainController;
    }

    public void startNewGame(){
        startTime = 0;
        activeGame = true;
        gameGenerator.generateTiles(numOfTilesX, numOfTilesY);
        gameGenerator.generatesMines(numOfMines, mainController);
        mainController.setTimeText("0", " / (PB: " + records.getBestTime(difficulty) + ")");
        mainController.drawTiles();
        mainController.adjustNodes();
        overallMinesMarked = 0;
    }

    public void gameOver(){
        activeGame = false;
        for(Tile tile : tileManager.getAllTiles()){
            mainController.updateTile(tile);
        }
        endTime = System.currentTimeMillis();
        mainController.getAnimationTimer().stop();
        mainController.setStatusText("GAME OVER!");
    }

    public void processTile(Tile tile){
        if(startTime == 0){
            mainController.getAnimationTimer().start();
            startTime = System.currentTimeMillis();
            //If easy mode is selected it will make sure that the first tile is a tile with 0
            if(easyMode){
                gameGenerator.adjustForEasyMode(tile, numOfMines);
            }
        }
        if(tile.isMine()){
            gameOver();
        }else if(tile.getNumberOfNeighboringMines() > 0){
            unveilTile(tile);
        }else{
            //It will unveil all tiles connected to this one which are not mines
            Stack<Tile> stack = new Stack<>();
            stack.push(tile);
            while(!stack.empty()){
                Tile currentTile = stack.pop();
                unveilTile(currentTile);
                //It will process a tile only if it has a value of 0
                if(currentTile.getNumberOfNeighboringMines() == 0){
                    for(Tile neighbour : currentTile.getNeighbours()){
                        if(neighbour.isClickable()){
                            stack.push(neighbour);
                        }
                    }
                }
            }
        }
    }

    public void unveilTile(Tile tile){
        tile.setClickable(false);
        mainController.updateTile(tile);
    }

    public void markTile(Tile tile){
        if(tile != null && tile.isClickable() && !tile.isMarked()){
            overallMinesMarked = overallMinesMarked + 1;
            tile.setClickable(false);
            tile.setMarked(true);
        }else if(tile != null && tile.isMarked()){
            overallMinesMarked = overallMinesMarked - 1;
            tile.setClickable(true);
            tile.setMarked(false);
        }
        mainController.updateTile(tile);
        mainController.setMinesText(Integer.toString(numOfMines - overallMinesMarked));
    }

    public void checkForVictory(){
        if(overallMinesMarked == numOfMines){
            for(Tile tile : tileManager.getAllTiles()){
                if(tile.isClickable()){
                    return;
                }
            }
            endTime = System.currentTimeMillis();
            activeGame = false;
            mainController.setStatusText("VICTORY!");
            checkHighScore();
        }
    }

    public void checkHighScore(){
        int elapsedMillis = (int)(endTime - startTime) / 1000;
        if(records.getBestTime(difficulty) > elapsedMillis || records.getBestTime(difficulty) == 0){
            records.setBestTime(difficulty, elapsedMillis);
        }
    }

    public boolean isActiveGame() {
        return activeGame;
    }

    public int getNumOfMines() {
        return numOfMines;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isEasyMode() {
        return easyMode;
    }

    public void setEasyMode(boolean easyMode) {
        this.easyMode = easyMode;
    }

    public TileManager getTileManager() {
        return tileManager;
    }

    public int getNumOfTilesX() {
        return numOfTilesX;
    }

    public void setNumOfTilesX(int numOfTilesX) {
        this.numOfTilesX = numOfTilesX;
    }

    public int getNumOfTilesY() {
        return numOfTilesY;
    }

    public void setNumOfTilesY(int numOfTilesY) {
        this.numOfTilesY = numOfTilesY;
    }

    public void setNumOfMines(int numOfMines) {
        this.numOfMines = numOfMines;
    }

    public Records getRecords() {
        return records;
    }
}
