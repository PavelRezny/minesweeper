package code;

import controller.MainController;

import java.util.ArrayList;
import java.util.List;

public class GameGenerator {

    private final TileManager tileManager;

    public GameGenerator(TileManager tileManager){
        this.tileManager = tileManager;
    }

    public void generateTiles(int numOfMinesX, int numOfMinesY){

        tileManager.getAllTiles().clear();

        for(int x = 0; x < numOfMinesX; x++){
            for(int y = 0; y < numOfMinesY; y++){
                tileManager.generateTile(x, y);
            }
        }
    }

    public void generatesMines(int numOfMines, MainController mainController){
        while(numOfMines > 0){
            Tile tile = tileManager.getRandomTile();
            if(!tile.isMine()){
                tile.setMine(true);
                numOfMines--;
            }
        }
        mainController.setMinesText(Integer.toString(numOfMines));
    }

    public void adjustForEasyMode(Tile tile, int numOfMines){
        //Free holds all tiles which will not have a mine
        List<Tile> free = new ArrayList<>();

        int minesToAdd = 0;

        if(tile.isMine()){
            tile.setMine(false);
            minesToAdd++;
        }

        free.add(tile);

        for(Tile neighbour : tile.getNeighbours()){
            //Will check if it is possible to make all neighbouring tiles to be mine-free
            if(numOfMines + tile.getNeighbours().size() < tileManager.getAllTiles().size()){
                if(neighbour.isMine()){
                    minesToAdd++;
                    neighbour.setMine(false);
                }
                free.add(neighbour);
            }
        }
        //Replace the removed mines
        while(minesToAdd > 0){
            Tile newTile = tileManager.getRandomTile();
            if(!newTile.isMine() && !free.contains(newTile)){
                newTile.setMine(true);
                minesToAdd--;
            }
        }
    }

}
