package code;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Tile{

    private final int x, y;
    private Rectangle rectangle;
    private boolean isMine, clickable = true, marked = false;

    private final List<Tile> neighbours = new ArrayList<>();

    public Tile(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Image getProperTileImage(boolean isGameActive){
        String path;
        if(!isGameActive){
            if(!isMine && marked){
                path = "/res/tileMarkedCrossed.png";
            }else if(isMine && !marked){
                path = "/res/tileMine.png";
            }else{
                return null;
            }
        }else if(marked){
            path = "/res/tileMarked.png";
        }else if(!clickable){
            path = "/res/tile" + getNumberOfNeighboringMines() + ".png";
        }else{
            path = "/res/tile.png";
        }
        return new Image(String.valueOf(Tile.class.getResource(path)));
    }

    public int getNumberOfNeighboringMines(){
        int amount = 0;
        for(Tile tile : neighbours){
            if(tile.isMine)
                amount++;
        }
        return amount;
    }

    public String toString(){
        return x + " " + y + " " + isMine;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean mine) {
        isMine = mine;
    }

    public List<Tile> getNeighbours() {
        return neighbours;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }
}
