package code;

import java.io.*;

public class Records {

    private int bestEasy = 0, bestMedium = 0, bestHard = 0;

    private final String path;

    public Records(){
        File dir = new File(System.getProperty("user.home") + "/Minesweeper");
        if(!dir.mkdir() && !dir.exists()){
            throw new RuntimeException("Failed to create /user/Minesweeper folder");
        }
        path = System.getProperty("user.home") + "/Minesweeper/data.txt";
    }

    public void readRecords(){
        File file = new File(path);
        if(!file.exists()){
            writeRecords();
        }else{
            try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
                String line;
                while ((line = reader.readLine()) != null){
                    Difficulty difficulty = Difficulty.valueOf(line.substring(0, line.indexOf(" ")));
                    setBestTime(difficulty, Integer.parseInt(line.substring(line.indexOf(" ") + 1)));
                }
            } catch (IOException e) {
                throw new RuntimeException("Failed to load data.txt file", e);
            }
        }
    }

    public void writeRecords(){
        try{
            FileWriter fw = new FileWriter(new File(path).getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("EASY " + getBestTime(Difficulty.EASY) + "\n");
            bw.write("MEDIUM " + getBestTime(Difficulty.MEDIUM) + "\n");
            bw.write("HARD " + getBestTime(Difficulty.HARD) + "\n");
            bw.flush();
            bw.close();
        }catch(IOException e){
            throw new RuntimeException("Failed to save to the data.txt file", e);
        }
    }

    public int getBestTime(Difficulty difficulty){
        if(difficulty == Difficulty.EASY){
            return bestEasy;
        }else if(difficulty == Difficulty.MEDIUM){
            return bestMedium;
        }else if(difficulty == Difficulty.HARD){
            return bestHard;
        }
        return -1;
    }

    public void setBestTime(Difficulty difficulty, int value){
        if(difficulty == Difficulty.EASY){
            bestEasy = value;
        }else if(difficulty == Difficulty.MEDIUM){
            bestMedium = value;
        }else if(difficulty == Difficulty.HARD){
            bestHard = value;
        }
        writeRecords();
    }

}
