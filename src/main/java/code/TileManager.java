package code;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TileManager {

    public static final int TILE_SIZE = 30;

    private final List<Tile> allTiles;

    public TileManager(){
        allTiles = new ArrayList<>();
    }

    public void generateTile(int x, int y){
        Tile tile = new Tile(x, y);
        setNeighbours(tile);
        allTiles.add(tile);
    }

    public void setNeighbours(Tile tile){
        for(int x = -1; x < 2; x++){
            for(int y = -1; y < 2; y++){
                if(x != 0 || y != 0){
                    Tile neighbour = getTile(tile.getX() + x, tile.getY() + y);
                    if(neighbour != null){
                        tile.getNeighbours().add(neighbour);
                        if(!neighbour.getNeighbours().contains(tile)){
                            neighbour.getNeighbours().add(tile);
                        }
                    }
                }
            }
        }
    }

    public Tile getTile(int x, int y){
        for(Tile tile : allTiles){
            if(tile.getX() == x && tile.getY() == y){
                return tile;
            }
        }
        return null;
    }

    public Tile getRandomTile(){
        return allTiles.get(ThreadLocalRandom.current().nextInt(allTiles.size()));
    }

    public List<Tile> getAllTiles() {
        return allTiles;
    }
}
